"use strict";

// 'use script';

var React = require('react');
var ReactDOM = require('react-dom');

var CommentBox = React.createClass({
	render: function()
	{
		return (
			<div className="commentBox">
				Hello, world! I am a CommentBox.
			</div>
		);
	}
});

ReactDOM.render(
	<CommentBox />,
	document.getElementById('content')
);

var Header = React.createClass({
	render: function()
	{
		return (
			<nav className="navbar navbar-inverse navbar-fixed-top">
				<div className="container">
					<div className="navbar-header">
					  <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span className="sr-only">Toggle navigation</span>
						<span className="icon-bar"></span>
						<span className="icon-bar"></span>
						<span className="icon-bar"></span>
					  </button>
					  <a className="navbar-brand" href="https://localhost:3050/app">Project name</a>
					</div>
					<div id="navbar" className="collapse navbar-collapse">
					  <ul className="nav navbar-nav">
						<li className="active"><a href="https://localhost:3050/app">Home</a></li>
						<li><a href="#about">About</a></li>
						<li><a href="#contact">Contact</a></li>
					  </ul>
					</div>
				</div>
			</nav>
		);
	}
});

ReactDOM.render(
	<Header />,
	document.getElementById('header')
);

// var AuthorizeButton = React.createClass({
	// getInitialState: function() 
	// {
		// return {authorized: false};
	// },
	// handleClick: function(event)
	// {
		// this.setState({authorized: !this.state.authorized});
		// window.location.href = "https://api.imgur.com/oauth2/authorize?client_id=2c607f42e50e4b6&response_type=code&state="+ makeState();
	// },
	// render: function()
	// {
		// return (
			// <button className="authorizeButton btn btn-primary" onClick={this.handleClick}>
				// Request Authorization
			// </button>
		// );
	// }
// });

// ReactDOM.render(
	// <AuthorizeButton />,
	// document.getElementById('authorize')
// );

// var AuthorizeButton = React.createClass({
	// getInitialState: function() 
	// {
		// return {authorized: false};
	// },
	// handleClick: function(event)
	// {
		// this.setState({authorized: !this.state.authorized});
		// window.location.href = "https://api.imgur.com/oauth2/authorize?client_id=2c607f42e50e4b6&response_type=code&state="+ makeState();
	// },
	// render: function()
	// {
		// return (
			// <button className="authorizeButton btn btn-primary" onClick={this.handleClick}>
				// Request Authorization
			// </button>
		// );
	// }
// });

// ReactDOM.render(
	// <AuthorizeButton />,
	// document.getElementById('authorize')
// );

var Footer = React.createClass({
	render: function()
	{
		return (
			<footer className="footer">
				<div className="container">

				<table className="table footerSection1">
					<tbody>
						<tr>
							<td>
								<img className="footerlogo"></img>
							</td>
							<td className="labels">
								About Us
							</td>
							<td className="labels">
								Contact Us
							</td>
							<td className="labels">
								Site Map
							</td>
							<td className="labels">
								Terms of Use
							</td>
							<td className="labels">
								Follow Us
								<table className="socialIcons">
									<tbody>
										<tr>
											<td>
												<i className="fa fa-twitter fa-lg"></i>
											</td>
											<td>
												<i className="fa fa-facebook fa-lg"></i>
											</td>
											<td>
												<i className="fa fa-google-plus fa-lg"></i>
											</td>
											<td>
												<i className="fa fa-youtube fa-lg"></i>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
				<table className="table footerSection2">
					<tbody>
					<tr>
						<td>
							Conditions of Use
						</td>
						<td>
							Privacy Notice
						</td>
						<td>
							© 2016, Heimerlode.com
						</td>
					</tr>
					</tbody>
				</table>
				</div>
			</footer>
		);
	}
});

ReactDOM.render(
	<Footer />,
	document.getElementById('footer')
);

// var ApiOptionsNew = React.createClass({
	// getInitialState:function()
	// {
		// // return {liked: false};
		// return {value: 'Hello!'};
	// },
	// handleChange: function(event)
	// // handleClick: function(event)
	// {
		// // this.setState({liked: !this.state.liked});
		// this.setState({value: event.target.value});
		// // console.log(event.target.value);
		// // $("#apiOptionsTemplate").append("heyyo");
	// },
	// render: function()
	// {
		// // var text = this.state.liked ? 'like': 'haven\'t liked';
		// var text = this.state.value;
		// // console.log(this.state.value);
		// return(
			// // <button onClick = {this.handleClick}>
				// // You {text} this. Click to toggle.
			// // </button>
			// // <select id="apiOptionSelected" onChange={this.handleChange} value={this.state.value} className="form-control">
			// <select id="apiOptionSelected" onChange={this.handleChange} value={text} className="form-control">
				// <option value="acctbase">Account Base</option>
				// <option value="acctgalleryfavs">Account Gallery Favorites</option>
				// <option value="acctfavs">Account Favorites</option>
				// <option value="acctsubmits">Account Submissions</option>
				// <option value="acctsettings">Account Settings</option>
				// <option value="changeacctsettings">Change Account Settings</option>
				// <option value="acctgalleryprofile">Account Gallery Profile</option>
				// <option value="sendverifyemail">Send Verification E-mail</option>
				// <option value="albums">Albums</option>
				// <option value="album">Album</option>
				// <option value="albumIds">Album IDs</option>
				// <option value="albumCount">Album Count</option>
				// <option value="albumDeletion">Album Deletion</option>
				// <option value="comments">Comments</option>
				// <option value="commentIds">Comment IDs</option>
				// <option value="commentCount">Comment Count</option>
				// <option value="commentDeletion">Comment Deletion</option>
				// <option value="images">Images</option>
				// <option value="imageIds">Image IDs</option>
				// <option value="imageCount">Image Count</option>
				// <option value="imageDeletion">Image Deletion</option>
				// <option value="replies">Replies</option>
			// </select>
			// // <div id="apiOptionSelected" onChange={this.handleChange} value={this.state.value}>{text}</div>
		// );
	// }
// });

// ReactDOM.render(
	// // <ApiOptionsNew />,
	// // document.getElementById('apiOptionsTest')
	// <ApiOptionsNew />,
	// document.getElementById('apiOptions_React')
// );

var ApiCategoriesNew = React.createClass({
	getInitialState:function()
	{
		// return {liked: false};
		return {value: 'Hello!'};
	},
	handleChange: function(event)
	{
		this.setState({value: event.target.value});
	},
	componentWillUpdate: function()
	{
		if ($("#apiCategorySelected").val() == "")
		{
			$("#apiCategoriesTemplate").html("");
			$("#apiOptionsTemplate").html("");
		}
		else if($("#apiCategorySelected").val() == "account")
		{
			$("#apiCategoriesTemplate").html("");
			$("#apiOptionsTemplate").html("");
			// $("#apiCategoriesTemplate").append('<select id="accountSelected" class="accountClass form-control">\
				// <option value=""></option>\
				// <option value="acctbase">Account Base</option>\
				// <option value="acctgalleryfavs">Account Gallery Favorites</option>\
				// <option value="acctfavs">Account Favorites</option>\
				// <option value="acctsubmits">Account Submissions</option>\
				// <option value="acctsettings">Account Settings</option>\
				// <option value="changeacctsettings">Change Account Settings</option>\
				// <option value="acctgalleryprofile">Account Gallery Profile</option>\
				// <option value="sendverifyemail">Send Verification E-mail</option>\
				// <option value="albums">Albums</option>\
				// <option value="album">Album</option>\
				// <option value="albumIds">Album IDs</option>\
				// <option value="albumCount">Album Count</option>\
				// <option value="albumDeletion">Album Deletion</option>\
				// <option value="comments">Comments</option>\
				// <option value="commentIds">Comment IDs</option>\
				// <option value="commentCount">Comment Count</option>\
				// <option value="commentDeletion">Comment Deletion</option>\
				// <option value="images">Images</option>\
				// <option value="imageIds">Image IDs</option>\
				// <option value="imageCount">Image Count</option>\
				// <option value="imageDeletion">Image Deletion</option>\
				// <option value="replies">Replies</option>\
				// </select>');
			
			$('<select />', {
				id: "accountSelected",
				class: "accountClass form-control"
			}).appendTo($("#apiCategoriesTemplate")).trigger("appended", ["accountAPI"]);

			$('<option value=""></option>').appendTo("#accountSelected");
			$('<option value="acctbase">Account Base</option>').appendTo("#accountSelected");
			$('<option value="acctgalleryfavs">Account Gallery Favorites</option>').appendTo("#accountSelected");
			$('<option value="acctfavs">Account Favorites</option>').appendTo("#accountSelected");
			$('<option value="acctsubmits">Account Submissions</option>').appendTo("#accountSelected");
			$('<option value="acctsettings">Account Settings</option>').appendTo("#accountSelected");
			$('<option value="changeacctsettings">Change Account Settings</option>').appendTo("#accountSelected");
			$('<option value="acctgalleryprofile">Account Gallery Profile</option>').appendTo("#accountSelected");
			$('<option value="sendverifyemail">Send Verification E-mail</option>').appendTo("#accountSelected");
			$('<option value="albums">Albums</option>').appendTo("#accountSelected");
			$('<option value="album">Album</option>').appendTo("#accountSelected");
			$('<option value="albumIds">Album IDs</option>').appendTo("#accountSelected");
			$('<option value="albumCount">Album Count</option>').appendTo("#accountSelected");
			$('<option value="albumDeletion">Album Deletion</option>').appendTo("#accountSelected");
			$('<option value="comments">Comments</option>').appendTo("#accountSelected");
			$('<option value="commentIds">Comment IDs</option>').appendTo("#accountSelected");
			$('<option value="commentCount">Comment Count</option>').appendTo("#accountSelected");
			$('<option value="commentDeletion">Comment Deletion</option>').appendTo("#accountSelected");
			$('<option value="images">Images</option>').appendTo("#accountSelected");
			$('<option value="imageIds">Image IDs</option>').appendTo("#accountSelected");
			$('<option value="imageCount">Image Count</option>').appendTo("#accountSelected");
			$('<option value="imageDeletion">Image Deletion</option>').appendTo("#accountSelected");
			$('<option value="replies">Replies</option>').appendTo("#accountSelected");
			console.log('success');
		}
		else if($("#apiCategorySelected").val() == "album")
		{
			$("#apiCategoriesTemplate").html("");
			$("#apiOptionsTemplate").html("");
			$('<select />', {
				id: "albumSelected",
				class: "albumClass form-control"
			}).appendTo($("#apiCategoriesTemplate")).trigger("appended", ["commentAPI"]);

			$('<option value=""></option>').appendTo("#albumSelected");
			$('<option value="album">Album</option>').appendTo("#albumSelected");
			$('<option value="albumImages">Album Images</option>').appendTo("#albumSelected");
			$('<option value="albumImage">Album Image</option>').appendTo("#albumSelected");
			$('<option value="albumCreation">Album Creation</option>').appendTo("#albumSelected");
			$('<option value="albumUpdate">Update Album</option>').appendTo("#albumSelected");
			$('<option value="albumDeletion">Album Deletion</option>').appendTo("#albumSelected");
			$('<option value="albumFavorite">Favorite Album</option>').appendTo("#albumSelected");
			$('<option value="setAlbumImages">Set Album Images</option>').appendTo("#albumSelected");
			$('<option value="addAlbumImages">Add Album Images</option>').appendTo("#albumSelected");
			$('<option value="removeAlbumImages">Remove Album Images</option>').appendTo("#albumSelected");
			console.log('success');
		}
		else if($("#apiCategorySelected").val() == "comment")
		{
			$("#apiCategoriesTemplate").html("");
			$("#apiOptionsTemplate").html("");
				
			$('<select />', {
				id: "commentSelected",
				class: "commentClass form-control"
			}).appendTo($("#apiCategoriesTemplate")).trigger("appended", ["commentAPI"]);
			
			$('<option value=""></option>').appendTo("#commentSelected");
			$('<option value="comment">Comment</option>').appendTo("#commentSelected");
			$('<option value="commentCreation">Comment Creation</option>').appendTo("#commentSelected");
			$('<option value="commentDeletion">Comment Deletion</option>').appendTo("#commentSelected");
			$('<option value="replies">Replies</option>').appendTo("#commentSelected");
			$('<option value="replyCreation">Reply Creation</option>').appendTo("#commentSelected");
			$('<option value="vote">Vote</option>').appendTo("#commentSelected");
			$('<option value="report">Report</option>').appendTo("#commentSelected");
			console.log('success');
		}
		else if($("#apiCategorySelected").val() == "customgallery")
		{
			$("#apiCategoriesTemplate").html("");
			$("#apiOptionsTemplate").html("");
				
			$('<select />', {
				id: "customgallerySelected",
				class: "customgalleryClass form-control"
			}).appendTo($("#apiCategoriesTemplate")).trigger("appended", ["customgalleryAPI"]);
			
			$('<option value=""></option>').appendTo("#customgallerySelected");
			$('<option value="customgallery">Custom Gallery</option>').appendTo("#customgallerySelected");
			$('<option value="filteredgallery">Filtered Out Gallery</option>').appendTo("#customgallerySelected");
			$('<option value="customgalleryimage">Custom Gallery Image</option>').appendTo("#customgallerySelected");
			$('<option value="customgalleryaddtags">Custom Gallery Add Tags</option>').appendTo("#customgallerySelected");
			$('<option value="customgalleryremovetags">Custom Gallery Remove Tags</option>').appendTo("#customgallerySelected");
			$('<option value="filteredoutgalleryadd">Filtered Out Gallery Add</option>').appendTo("#customgallerySelected");
			$('<option value="filteredoutgalleryadd">Filtered Out Gallery Remove</option>').appendTo("#customgallerySelected");
			console.log('success');
		}
		else if($("#apiCategorySelected").val() == "gallery")
		{
			$("#apiCategoriesTemplate").html("");
			$("#apiOptionsTemplate").html("");
				
			$('<select />', {
				id: "gallerySelected",
				class: "galleryClass form-control"
			}).appendTo($("#apiCategoriesTemplate")).trigger("appended", ["galleryAPI"]);
			
			$('<option value=""></option>').appendTo("#gallerySelected");
			$('<option value="gallery">Gallery</option>').appendTo("#gallerySelected");
			$('<option value="memesubgallery">Memes Subgallery</option>').appendTo("#gallerySelected");
			$('<option value="memesubgalleryimage">Memes Subgallery Image</option>').appendTo("#gallerySelected");
			$('<option value="gallerytag">Gallery Tag</option>').appendTo("#gallerySelected");
			$('<option value="gallerytagimage">Gallery Tag Image</option>').appendTo("#gallerySelected");
			$('<option value="galleryitemtags">Gallery Item Tags</option>').appendTo("#gallerySelected");
			$('<option value="gallerytagvoting">Gallery Tag Voting</option>').appendTo("#gallerySelected");
			$('<option value="gallerysearch">Gallery Search</option>').appendTo("#gallerySelected");
			$('<option value="randomgalleryimages">Random Gallery Images</option>').appendTo("#gallerySelected");
			$('<option value="sharewithcommunity">Share With Community</option>').appendTo("#gallerySelected");
			$('<option value="removeFromGallery">Remove From Gallery</option>').appendTo("#gallerySelected");
			$('<option value="album">Album</option>').appendTo("#gallerySelected");
			$('<option value="image">Image</option>').appendTo("#gallerySelected");
			$('<option value="albumimagereporting">Album/Image Reporting</option>').appendTo("#gallerySelected");
			$('<option value="albumimagevotes">Album/Image Votes</option>').appendTo("#gallerySelected");
			$('<option value="albumimagevoting">Album/Image Voting</option>').appendTo("#gallerySelected");
			$('<option value="albumimagecomments">Album/Image Comments</option>').appendTo("#gallerySelected");
			$('<option value="albumimagecomment">Album/Image Comment</option>').appendTo("#gallerySelected");
			$('<option value="albumimagecommentcreation">Album/Image Comment Creation</option>').appendTo("#gallerySelected");
			$('<option value="albumimagecommentreplycreation">Album/Image Comment Reply Creation</option>').appendTo("#gallerySelected");
			$('<option value="albumimagecommentids">Album/Image Comment IDs</option>').appendTo("#gallerySelected");
			$('<option value="albumimagecommentcount">Album/Image Comment Count</option>').appendTo("#gallerySelected");
			console.log('success');
		}
		else if($("#apiCategorySelected").val() == "image")
		{
			$("#apiCategoriesTemplate").html("");
			$("#apiOptionsTemplate").html("");
				
			$('<select />', {
				id: "imageSelected",
				class: "imageClass form-control"
			}).appendTo($("#apiCategoriesTemplate")).trigger("appended", ["imageAPI"]);
			
			$('<option value=""></option>').appendTo("#imageSelected");
			$('<option value="image">Image</option>').appendTo("#imageSelected");
			$('<option value="imageupload">Image Upload</option>').appendTo("#imageSelected");
			$('<option value="imagedeletion">Image Deletion</option>').appendTo("#imageSelected");
			$('<option value="imageupdateinfo">Update Image Information</option>').appendTo("#imageSelected");
			$('<option value="imagefavorite">Favorite an Image</option>').appendTo("#imageSelected");
			console.log('success');
		}
		else if($("#apiCategorySelected").val() == "conversation")
		{
			$("#apiCategoriesTemplate").html("");
			$("#apiOptionsTemplate").html("");
			$('<select />', {
				id: "conversationSelected",
				class: "conversationClass form-control"
			}).appendTo($("#apiCategoriesTemplate")).trigger("appended", ["conversationAPI"]);

			$('<option value=""></option>').appendTo("#conversationSelected");
			$('<option value="conversationList">Conversation List</option>').appendTo("#conversationSelected");
			$('<option value="conversation">Conversation</option>').appendTo("#conversationSelected");
			$('<option value="messagecreation">Message Creation</option>').appendTo("#conversationSelected");
			$('<option value="conversationdeletion">Conversation Deletion</option>').appendTo("#conversationSelected");
			$('<option value="reportsender">Report Sender</option>').appendTo("#conversationSelected");
			$('<option value="blocksender">Block Sender</option>').appendTo("#conversationSelected");
			console.log('success');
		}
		else if($("#apiCategorySelected").val() == "notification")
		{
			$("#apiCategoriesTemplate").html("");
			$("#apiOptionsTemplate").html("");
			$('<select />', {
				id: "notificationSelected",
				class: "notificationClass form-control"
			}).appendTo($("#apiCategoriesTemplate")).trigger("appended", ["notificationAPI"]);

			$('<option value=""></option>').appendTo("#notificationSelected");
			$('<option value="notifications">Notifications</option>').appendTo("#notificationSelected");
			$('<option value="notification">Notification</option>').appendTo("#notificationSelected");
			$('<option value="notificationviewed">Notification Viewed</option>').appendTo("#notificationSelected");
			console.log('success');
		}
		else if($("#apiCategorySelected").val() == "memegen")
		{
			$("#apiCategoriesTemplate").html("");
			$("#apiOptionsTemplate").html("");
			$('<select />', {
				id: "memegenSelected",
				class: "memegenClass form-control"
			}).appendTo($("#apiCategoriesTemplate")).trigger("appended", ["memegenAPI"]);

			$('<option value=""></option>').appendTo("#memegenSelected");
			$('<option value="defaultmemes">Default Memes</option>').appendTo("#memegenSelected");
			console.log('success');
		}
		else if($("#apiCategorySelected").val() == "topic")
		{
			$("#apiCategoriesTemplate").html("");
			$("#apiOptionsTemplate").html("");
			$('<select />', {
				id: "topicSelected",
				class: "topicClass form-control"
			}).appendTo($("#apiCategoriesTemplate")).trigger("appended", ["topicAPI"]);

			$('<option value=""></option>').appendTo("#topicSelected");
			$('<option value="defaulttopics">Default Topics</option>').appendTo("#topicSelected");
			$('<option value="gallerytopic">Gallery Topic</option>').appendTo("#topicSelected");
			$('<option value="gallerytopicitem">Gallery Topic Item</option>').appendTo("#topicSelected");
			console.log('success');
		}
		else {}
	},
	render: function()
	{
		var text = this.state.value;
		return(
			// <button onClick = {this.handleClick}>
				// You {text} this. Click to toggle.
			// </button>
			<select id="apiCategorySelected" onChange={this.handleChange} value={text} className="form-control">
				<option value=""></option>
				<option value="account">Account</option>
				<option value="album">Album</option>
				<option value="comment">Comment</option>
				<option value="customgallery">Custom Gallery</option>
				<option value="gallery">Gallery</option>
				<option value="image">Image</option>
				<option value="conversation">Conversation</option>
				<option value="notification">Notification</option>
				<option value="memegen">Memegen</option>
				<option value="topic">Topic</option>
			</select>
		);
	}
});

ReactDOM.render(
	<ApiCategoriesNew />,
	document.getElementById('apiCategories_React')
);

// var ApiOptionsNew = React.createClass({
	// getInitialState:function()
	// {
		// // return {liked: false};
		// return {value: 'Hello!'};
	// },
	// handleChange: function(event)
	// // handleClick: function(event)
	// {
		// // this.setState({liked: !this.state.liked});
		// this.setState({value: event.target.value});
		// // console.log(event.target.value);
		// // $("#apiOptionsTemplate").append("heyyo");
	// },
	// render: function()
	// {
		// // var text = this.state.liked ? 'like': 'haven\'t liked';
		// var text = this.state.value;
		// // console.log(this.state.value);
		// return(
			// // <button onClick = {this.handleClick}>
				// // You {text} this. Click to toggle.
			// // </button>
			// // <select id="apiOptionSelected" onChange={this.handleChange} value={this.state.value} className="form-control">
			// <select id="apiOptionSelected" onChange={this.handleChange} value={text} className="form-control">
				// <option value="acctbase">Account Base</option>
				// <option value="acctgalleryfavs">Account Gallery Favorites</option>
				// <option value="acctfavs">Account Favorites</option>
				// <option value="acctsubmits">Account Submissions</option>
				// <option value="acctsettings">Account Settings</option>
				// <option value="changeacctsettings">Change Account Settings</option>
				// <option value="acctgalleryprofile">Account Gallery Profile</option>
				// <option value="sendverifyemail">Send Verification E-mail</option>
				// <option value="albums">Albums</option>
				// <option value="album">Album</option>
				// <option value="albumIds">Album IDs</option>
				// <option value="albumCount">Album Count</option>
				// <option value="albumDeletion">Album Deletion</option>
				// <option value="comments">Comments</option>
				// <option value="commentIds">Comment IDs</option>
				// <option value="commentCount">Comment Count</option>
				// <option value="commentDeletion">Comment Deletion</option>
				// <option value="images">Images</option>
				// <option value="imageIds">Image IDs</option>
				// <option value="imageCount">Image Count</option>
				// <option value="imageDeletion">Image Deletion</option>
				// <option value="replies">Replies</option>
			// </select>
			// // <div id="apiOptionSelected" onChange={this.handleChange} value={this.state.value}>{text}</div>
		// );
	// }
// });

// ReactDOM.render(
	// <ApiOptionsNew />,
	// document.getElementById('apiOptions_React')
// );