var webpack = require('webpack');
var path = require('path');

module.exports = {
	entry: "./app/js/input.js",
	output: {
		filename: "./app/js/bundle.js"
	},
	module: {
	  loaders: [
		{
		  test: /\.jsx?$/,
		  exclude: /(node_modules|bower_components)/,
		  loader: 'babel-loader', // 'babel-loader' is also a legal name to reference
		  query: {
			presets: ['es2015', 'react'],
			cacheDirectory: true,
		  }
		}
	  ]
	},
	node: {
		fs: 'empty',
		module: 'empty',
		net: 'empty'
	},
	resolve: {
		root: path.join(__dirname),
		fallback: path.join(__dirname, 'node_modules'),
		modulesDirectories: ['node_modules'],
		extensions: ['', '.js',',jsx', '.scss', '.png', '.jpg', '.jpeg', '.gif'],
	}
}