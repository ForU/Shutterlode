var express = require('express');

var request = require('request');

var http = require('http');
var https = require('https');
var fs = require('fs');
var cors = require('cors');

var options = {
	key: fs.readFileSync(__dirname + '/ssl_certs/private.key'),
	cert: fs.readFileSync(__dirname + '/ssl_certs/public.crt')
	// ca: fs.readFileSync('C:/wamp/bin/php/php5.5.12/cacert.pem')
};

var app = express();

// This works
app.listen(3000, function() {
	console.log('Example app listening on port 3000!');
});

// app.use("/react", express.static('react'));
app.use("/node_modules", express.static('node_modules'));
app.use("/bootstrap", express.static('bootstrap'));
app.use("/app", express.static('app'));

app.get('/*', function(req, res) {
	res.sendFile(__dirname + '/app/index.html');
});

var httpsServer = https.createServer(options, app).listen(3050);

// var httpsServer = https.createServer(options, app);
// httpsServer.listen(3040, function() {
	// console.log('Example app listening on port 3040!');
// });

// var httpServer = http.createServer(app);
// httpServer.listen(3030);